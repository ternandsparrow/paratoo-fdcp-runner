# Philosophy of this repo

This repo contains only the code/tools needed to run/deploy the Paratoo FDCP
system. It is intended to be used by anyone who wants to deploy the system, not
just Paratoo developers. Let's call this collection of people "Paratoo
Deployers".

We use a git repository for two reasons:
  - all the usual benefits of tracking code in version control
  - we get a free mechanism for distributing updates to Paratoo Deployers

That second point has certain implications for how developers must work with
this repository:

- the `main` branch is *always* production ready. Paratoo Deployers can update
  their deployments at any time by doing `git pull`. This *is* the update
  mechanism mentioned above
- Developers should *not* commit directly to `main`. Instead, they should work
    from feature branches that are merged into `develop`. The `main` branch in
    the `paratoo-fdcp` repo is then updated to point (the submodule) at this new
    commit (git submodules always point to a specific commit, not a branch).
    Once this is all pushed, the CI/CD pipeline of `paratoo-fdcp` will update
    the `main` branch of this repo once the images have been built and pushed
    successfully.
- any branch other than `main` (e.g. `develop`) should be considered unstable
- only ask the user to keep the minimum amount of config in `local.env`. As much
  as possible should be smart defaults or dynamically detected, so upgrades are
  easier (for Paratoo Deployers). This is why we have the `paratoo` script as the
  command to interact with the stack. It lets paratoo developers codify rules
  about how the stack should operate.
