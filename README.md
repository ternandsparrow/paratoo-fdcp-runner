> This repository contains the tools needed to run the Paratoo FDCP system.

The source code for the docker images used in this stack lives in a separate git
repository: https://gitlab.com/ternandsparrow/paratoo-fdcp.

# Quickstart - public deployment
The term *public* means you have a DNS record and your server is reachable from
the public internet, specifically the LetsEncrypt verification servers, so we
can mint HTTPS certificates.

  1. clone this repo. The benefit of cloning the whole git repo (as opposed to
     only grabbing the files) is in the future you'll be able to use `git pull` to
     get updates to the stack.
  1. get a Linux VM to run Paratoo FDCP on
  1. install [`docker`](https://docs.docker.com/engine/install/#server) (version
     20.10 or newer) and
     [`docker-compose`](https://docs.docker.com/compose/install/) (version
     1.28.2 or newer)
  1. read `.env` for an understanding about what config *can* be overridden, and
     what each config item means. The rest of these instructions will tell you
     the minimum config you need to override.
  1. create the user config override file. See further down in this document for
     more info about `local.env`.
      ```bash
      touch local.env
      ```
  1. disable "local dev" mode and set a secure DB password
      ```bash
      echo PARATOO_ENABLE_LOCAL_DEV_MODE=0 >> local.env
      echo DB_PASSWORD=changeme >> local.dev # change the value
      ```
  1. set details used for routing and minting HTTPS certificates (via
     LetsEncrypt)
      ```bash
      echo LETSENCRYPT_EMAIL=changeme@example.com >> local.env # change the value
      echo DNS_PARATOO_CORE=core-api.paratoo.your.host >> local.env # change the value
      # this value can be anything, as long as it's 32 chars or longer
      echo CORE_STRAPI_SECRET=some-good-secret-for-oidc-thats-gte-32-chars >> local.env # change the value
      ```
  1. go to the [container
     registry](https://gitlab.com/ternandsparrow/paratoo-fdcp/container_registry)
     for Paratoo and select a tag (version) of the core image. If you're not
     sure, pick the newest one. Don't use `latest`!
     ([why](https://vsupalov.com/docker-latest-tag/)).
      ```bash
      echo CORE_TAG=202102090302.a986a8e7 >> local.env # change the value
      ```
  1. if you want to run non-production Docker images, because you're a paratoo
     developer, you can set some config to use the `develop` or `beta` images by
     setting:
      ```bash
      echo IMAGE_ENV=/develop >> local.env # or,
      # echo IMAGE_ENV=/beta >> local.env
      ```
  1. using the "paratoo-core" component is non-optional. As the name suggests,
     it's the core of the system. You *do* have a choice if you want to use
     other system components: the paratoo-org (user and project management) and
     webapp (client for collecting data). The Paratoo team develop ready to use
     implementations of these components but if you have your own user
     management system and want to build your own UI, that's fine too. Let's
     make that choice (choose the first OR second block):
      ```bash
      # YES, use all the pre-built components from the paratoo team
      echo DNS_PARATOO_ORG=org-api.paratoo.your.host >> local.env # change the value
      echo DNS_WEBAPP=app.paratoo.your.host >> local.env # change the value
      # this value can be anything, as long as it's 32 chars or longer
      echo ORG_STRAPI_SECRET=some-other-secret-for-oidc-thats-gte-32-chars >> local.env # change the value
      # you can find tags for these images from the same spot as the core image
      # in a previous step.
      echo ORG_TAG=202102090302.a986a8e7 >> local.env # change the value
      echo WEBAPP_TAG=202102090302.a986a8e7 >> local.env # change the value
      ```
      or
      ```bash
      # NO, I'll build my own components. View the
      # https://gitlab.com/ternandsparrow/paratoo-fdcp/-/blob/main/ARCHITECTURE/org-interface.md
      # doc to see what's required of you.
      echo PARATOO_ENABLE_ORG_WEBAPP_MODE=1 >> local.env
      # you need to configure where your implementation of paratoo-org is
      echo ORG_URL_PREFIX=https://your.server:1234 >> local.env # change the value
      # no config is required for the webapp/client as the client points to the
      # core, not vice versa.
      ```
  1. when files (photos, etc) are uploaded to paratoo-core, they will be stored
     on AWS S3. If you don't already have an AWS S3 bucket to use, create one
     that allows public objects and create an IAM user that has the following
     policy assigned (update the bucket name):
      ```json
      {
        "Version": "2012-10-17",
          "Statement": [
            {
              "Sid": "VisualEditor0",
              "Effect": "Allow",
              "Action": [
                "s3:PutObject",
                "s3:GetObjectAcl",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:DeleteObject",
                "s3:PutObjectAcl"
              ],
              "Resource": [
                "arn:aws:s3:::<your-bucket-name>/*",
                "arn:aws:s3:::<your-bucket-name>"
              ]
            }
          ]
      }
      ```
  1. add the config for the S3 bucket and user you just created
      ```bash
      echo S3_ACCESS_KEY=AKIAAAAAAAAAAAAAAAAA >> local.env # change the value
      echo S3_SECRET_KEY=nMXYaaaaaaaaaaaaa... >> local.env # change the value
      echo S3_BUCKET_NAME=somebucket >> local.env # change the value
      ```
  1. now you can start the stack
      ```bash
      ./paratoo
      ```
  1. create an admin user for Paratoo Core, by opening your browser to
     https://core-api.paratoo.your.host/admin (or whatever you changed the hostname to)
     and filling in the form. You should do this immediately as the registration
     is open and the first user to the page will become the superuser!
  1. if you have org+webapp mode on, create another admin user for Paratoo Org,
     by opening your browser to https://org-api.paratoo.your.host/admin (or
     whatever you changed the hostname to) and filling in the form
  1. if you did *not* disable org+webapp mode, open the webapp at
     https://app.paratoo.your.host/ (or whatever you changed the hostname to).
     You can create a user directly from the login page, or via the Paratoo Org
     admin console that you opened in a previous step

# Updating the stack
The Paratoo team publish the components of the system as docker images. That
coupled with the fact that this is a git repository mean updates are fairly
easy:

  1. get the newest version of the docker-compose files and tools
      ```bash
      git pull
      ```
  1. restart the stack with the latest images
      ```bash
      ./paratoo
      ```
  1. if any new mandatory configuration has been introduced, you'll be prompted
     to define those values before you can continue

# The `local.env` file
This file is where you store your configuration overrides and it *must* have
this filename! This file is ignored from version control so secrets are safe
here and updates won't clobber your changes.

Any configuration you find in `.env` can be override in `local.env`. Only
override the bare minimum config so when you update the stack in the future, you
won't shadow new default values.

**Important** DO NOT `cp .env local.env`. Just create an empty `local.env` and
add the config you require. For local dev, you might not even need this file.

# Quickstart - developers
**Important**: do **NOT** work on or `git push` the `main` branch. Always use
another branch (`develop` for instance) to do your work. See
[ARCHITECTURE.md](./ARCHITECTURE.md) for more details.

If you're developing this stack, you should edit this repository in conjuction
with changes to https://gitlab.com/ternandsparrow/paratoo-fdcp/. If you're
already viewing this as a submodule of that repo, you have a choice about how to
work with *this* repo:

  1. clone this repo separately and make changes as you would with any repo
  1. change the submodule so you can work directly on the submodule, namely `git
     push`

To do the latter option, you need to set the url for the `origin` remote to use
SSH, rather than the default HTTPS. If you're using saved username/password and
HTTPS URLs, consider swapping to using SSH instead. The command to run:

```bash
cd paratoo-fdcp-runner
git remote set-url origin git@gitlab.com:ternandsparrow/paratoo-fdcp-runner.git
```

Note: the change of URL doesn't affect the parent, it's only local config for
you.

**Beware** of a few things:
- that some operations in the parent git repo could lose uncommited changes in
  the submodule
- when you go to make changes to the submodule, make sure you're *on* a branch.
    If you're in detatched head mode, your commit won't be lost but it won't
    push to the remote like you expect. If you do commit in detatched head mode,
    note the commit hash (`aaabbbcc`) and then `git checkout abranch` and `git
    cherry-pick aaabbbcc` to move that commit onto the branch.

## Start the stack in local dev mode

  1. the default config already set up for this. The following command expects
     this repo is a submodule of the `paratoo-fdcp` project as we're going to
     build the docker images in that parent repo.
      ```bash
      ./paratoo
      ```
  1. See further down in this document for more info about `local.env`, which is
     a file you can use to set your own config.
  1. as we're running in local dev mode (no publicly accessible domain), Caddy
     will mint HTTPS certificates for you using its internal CA (AKA snake-oil
     certificates). Your browser **WILL NOT** trust these certificates (and
     that's expected) so you'll have to add an exception for them. In some
     browsers that seems to reset each day.  Beware if you're seeing failed HTTP
     calls from the web client to the API, it's simply from your browser not
     having the exception for the cert today.
  1. follow the last few steps of the "Quickstart - public deployment" section
     above on how to create admins and users.


# Local user config `local.env`
The `local.env` is not tracked by version control. You only need to create it if
you want to override the default config. To see what config keys are available
to be overridden, look at `.env`. The file goes into detail about what each
value means.

# Build failures on OSX
Docker version 20.10.5, build 55c4c88 on OSX fails with weird error messages
https://stackoverflow.com/questions/64221861/failed-to-resolve-with-frontend-dockerfile-v0
Bypass BUILDKIT to move forward

export DOCKER_BUILDKIT=0
export COMPOSE_DOCKER_CLI_BUILD=0

