import { spawn } from "child_process";
import "./log.mjs";
/**
 * function to run bash command
 * @param {*} cmd
 * @returns
 */
function run(cmd) {
  if (process.env.DRY_RUN) {
    console.log(cmd);
    return;
  }
  const promise = new Promise((resolve, reject) => {
    let stdout = "";
    let stderr = "";
    const child = spawn(cmd, { shell: "/bin/bash" });
    child.stdout.setEncoding("utf8");
    let prevData = [];
    child.stdout.on("data", (data) => {
      const curData = data?.toString().trim();
      if (curData && !prevData.includes(curData)) {
        prevData.push(curData);
        if (prevData.length > 5) prevData.shift();
        console.log(curData);
      }
      stdout += data.toString();
    });

    child.stderr.setEncoding("utf8");
    child.stderr.on("data", (error) => (stderr += error.toString()));

    child.on("close", (code) => {
      if (code !== 0) {
        reject(stderr);
      } else {
        resolve(stdout);
      }
    });
  });
  return promise;
}

export default run;
