import chalk from "chalk";

const red = chalk.bold.red;
const blue = chalk.bold.blue;
const yellow = chalk.bold.yellow;

const originalError = console.error;
const originalLog = console.log;
const originalWarn = console.warn;

console.error = function () {
  originalError.call(console, red("[ERROR]"), ...arguments);
};

console.log = function () {
  originalLog.call(console, blue("[INFO]"), ...arguments);
};

console.warn = function () {
  originalWarn.call(console, yellow("[WARN]"), ...arguments);
};
