import "./log.mjs";
import * as readline from "node:readline/promises";
import { stdin as input, stdout as output } from "node:process";

export default async function question(text) {
  const rl = readline.createInterface({ input, output });
  try {
    const answer = await rl.question(text);
    return answer;
  } catch (error) {
    console.error(error?.message || error);
    process.exit(1);
  } finally {
    rl.close();
  }
}
