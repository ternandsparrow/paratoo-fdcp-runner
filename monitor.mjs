#!/usr/bin/env -S bash -c 'cd "$(dirname "$0")" && yarn node "$(basename "$0")" $@'
"use strict";
import { config } from "dotenv";
import path from "path";
import "./misc/log.mjs";
import { dirname } from "node:path";
import { fileURLToPath } from "node:url";
import fs from "fs/promises";
import run from "./misc/run.mjs";
import question from "./misc/question.mjs";

const __dirname = dirname(fileURLToPath(import.meta.url));

const SERVICES = ["core", "webapp", "exporter"];

async function init() {
  await loadConfig();
  await checkParams();
  console.log("done!");
  process.exit(0);
}

async function checkParams() {
  const param = process.argv[2] || "";

  switch (param) {
    case "-h":
    case "--help":
    case "help":
      showHelp();
      break;

    case "ls":
      await run("docker service ls");
      break;

    case "logs":
      await logs();
      break;

    case "init":
      await initStack();
      break;

    case "":
    case "up":
      await deployStack();
      break;

    case "update": {
      await updateStack();
      break;
    }

    case "down": {
      const confirm = await question(
        "[Confirm] Do you want to remove the stack[y/n]?"
      );
      if (["N", "n", "no"].includes(confirm)) process.exit();
      console.log("Removing the stack");
      try {
        await run(`docker stack rm monitor_stack`);
      } catch {
        console.error("fail to bring down the stack");
      }
      break;
    }

    case "leave":
      console.log("Leaving the swarm");
      await run(`docker swarm leave --force`);
      break;

    default:
      console.error(`command ${param} not found`);
      break;
  }
}

function showHelp() {
  console.log(`Usage:
    Run with no args to start/update the whole stack
      ./monitor
    TO update webapp service, 
      ./monitor update webapp
    TO update core service, 
      ./monitor update core
    To stop the swarm
      ./monitor down
    To leave the swarm
      ./monitor leave
    To init the swarm
      ./monitor init
    To see the logs 
      ./monitor logs  [options] [service]
  `);
  process.exit();
}

async function logs() {
  let args = "";
  let selectedService;
  for (let i = 3; i < process.argv.length; i++) {
    if (!process.argv[i].startsWith("-") && !selectedService) {
      for (const service of SERVICES) {
        if (process.argv[i].includes(service)) {
          selectedService = service;
          break;
        }
      }
      continue;
    }
    args += ` ${process.argv[i]}`;
  }
  try {
    await run(`docker service logs ${args} monitor_stack_${selectedService}`);
  } catch (error) {
    console.error(error);
  }
}

async function initStack() {
  console.log("Initializing the swarm");
  try {
    await run(`docker swarm init`);
    await run(`docker swarm join-token manager > ${__dirname}/manager_token`);
    await run(`docker swarm join-token worker > ${__dirname}/worker_token`);
    console.log(`Your manager token was saved in ${__dirname}/manager_token`);
    console.log(`Your worker token was saved in ${__dirname}/worker_token`);
  } catch (error) {
    console.error(error);
  }
}

async function deployStack() {
  try {
    const lsCmd = `docker service ls --format "{{.Name}}" | grep monitor_ | wc -l`;
    const lsOutput = await run(lsCmd);
    const numServices = parseInt(lsOutput.trim());
    if (numServices > 0) {
      console.log("Updating the stack");
      const confirm = await question(
        "[CONFIRM] Do you want to update the stack [all services will be restarted][y/n]? "
      );
      if (["N", "n", "no"].includes(confirm)) process.exit();
    }
  } catch (error) {
    console.error(error);
  }

  console.log("Deploying the stack");
  // load env variables
  for (const service of SERVICES) {
    await mapEnvVariables(service);
  }
  await runDockerDeployCommand();
}

async function updateStack() {
  const serviceToUpdate = process.argv[3];
  if (!SERVICES.includes(serviceToUpdate)) {
    console.error("Update fail, unknown service");
    process.exit(1);
  }

  const image_env = process.env.IMAGE_ENV || "";
  const imgEnvs = {
    core: "CORE_TAG",
    webapp: "WEBAPP_TAG",
    exporter: "ABIS_EXPORTER_IMG",
  };

  // image repo
  const tag = process.env[imgEnvs[serviceToUpdate]] || "latest";
  let img_path = serviceToUpdate;
  if (serviceToUpdate === "exporter") {
    img_path = "abis_exporter";
  }
  const img = `registry.gitlab.com/ternandsparrow/paratoo-fdcp/${img_path}${image_env}:${tag}`;

  // env vars to update
  const envArgs = await mapEnvVariables(serviceToUpdate);

  const updateCommand = `docker service update  --force --image "${img}" ${envArgs} monitor_stack_${serviceToUpdate}`;
  try {
    console.log(`updating ${serviceToUpdate}`);
    await run(updateCommand);
  } catch (error) {
    console.error("update failed with error");
    console.error(error);
  }
}

async function runDockerDeployCommand() {
  let ymlFiles = `--compose-file ${__dirname}/docker-swarm.core.yml`;
  if (process.env.PARATOO_ENABLE_WEBAPP_MODE > 0) {
    ymlFiles += ` --compose-file ${__dirname}/docker-swarm.webapp.yml`;
  }

  if (process.env.PARATOO_ENABLE_DATA_EXPORT_MODE > 0) {
    ymlFiles += ` --compose-file ${__dirname}/docker-swarm.data-export.yml`;
  }
  const command = `docker stack deploy ${ymlFiles} monitor_stack --detach=True`;
  try {
    await run(command);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

/**
 * Load env files
 */
async function loadConfig() {
  const localEnv = path.join(__dirname, "local.env");
  config();
  try {
    const pathExist = await fs.stat(localEnv);
    if (pathExist) {
      console.log("local.env found, adding local config");
      config({ path: localEnv, override: true });
    }
  } catch {
    console.log("local config not found using .env");
  }
}

// work around to allow updating a single service using env file
// although 'docker stack deploy' can use .env file with docker-compose file but 'docker service update' does not (╯°□°）╯︵ ┻━┻
// and some variables does not have the same name so we have to map it to a correct one then concat them to a cli command
/**
 *
 * @param {String} string core|org|webapp|exporter
 */
async function mapEnvVariables(service = null) {
  const envMap = {
    core: {
      DATABASE_HOST: "CORE_DB_HOST",
      DATABASE_NAME: "CORE_DB_NAME",
      DATABASE_USERNAME: "DB_USER",
      DATABASE_PASSWORD: "DB_PASSWORD",
      ADMIN_JWT_SECRET: "CORE_STRAPI_ADMIN_SECRET",
      JWT_SECRET: "CORE_STRAPI_SECRET",
      API_TOKEN_SALT: "CORE_API_TOKEN_SALT",
      ADMIN_FNAME: "ADMIN_FNAME",
      ADMIN_LNAME: "ADMIN_LNAME",
      ADMIN_USER: "ADMIN_USER",
      ADMIN_EMAIL: "ADMIN_EMAIL",
      ADMIN_PASS: "ADMIN_PASS",
      DATABASE_SSL: "CORE_DATABASE_SSL",
      DATABASE_SSL_REJECT_UNAUTHORIZED: "CORE_DB_SSL_REJECT_UNAUTHORIZED",
      DATABASE_ACQUIRE_TIMEOUT: "CORE_DATABASE_ACQUIRE_TIMEOUT",
      DATABASE_POOL_MIN: "CORE_DATABASE_POOL_MIN",
      DATABASE_POOL_MAX: "CORE_DATABASE_POOL_MAX",
      DATABASE_CREATE_TIMEOUT: "CORE_DATABASE_CREATE_TIMEOUT",
      DATABASE_IDLE_TIMEOUT: "CORE_DATABASE_IDLE_TIMEOUT",
      DATABASE_REAP_INTERVAL: "CORE_DATABASE_REAP_INTERVAL",
      DATABASE_CREATE_RETRY_INTERVAL: "CORE_DATABASE_CREATE_RETRY_INTERVAL",
      DATABASE_ACQUIRE_CONNECTION_TIMEOUT:
        "CORE_DATABASE_ACQUIRE_CONNECTION_TIMEOUT",
      AWS_ACCESS_KEY_ID: "S3_ACCESS_KEY",
      AWS_ACCESS_SECRET: "S3_SECRET_KEY",
      S3_REGION: "S3_REGION",
      S3_BUCKET: "S3_BUCKET_NAME",
      ORG_URL_PREFIX: "ORG_URL_PREFIX",
      DNS_PARATOO_CORE: "DNS_PARATOO_CORE",
      SENTRY_DSN: "CORE_SENTRY_DSN",
      STRAPI_LOG_LEVEL: "CORE_STRAPI_LOG_LEVEL",
      DRONE_ACCESS_KEY: "DRONE_ACCESS_KEY",
      PARATOO_ACCESS_KEY: "PARATOO_ACCESS_KEY",
      ORG_ENABLE_TOKEN_VALIDATION: "ORG_ENABLE_TOKEN_VALIDATION",
      ORG_API_PREFIX: "ORG_API_PREFIX",
      ORG_CUSTOM_API_PREFIX: "ORG_CUSTOM_API_PREFIX",
      EXPORTER_JWT: "EXPORTER_JWT",
      CRON_EXPORTER: "CRON_EXPORTER",
      ENABLE_CRON_EXPORTER_OIDC: "ENABLE_CRON_EXPORTER_OIDC",
      ABIS_S3_SCHEDULE: "ABIS_S3_SCHEDULE",
      CRON_PUSH_MOCK_COLLECTION: "CRON_PUSH_MOCK_COLLECTION",
      MOCK_PROJECT_ID: "MOCK_PROJECT_ID",
      MOCK_ORG_EMAIL: "MOCK_ORG_EMAIL",
      MOCK_ORG_USER: "MOCK_ORG_USER",
      MOCK_ORG_PASSWORD: "MOCK_ORG_PASSWORD",
      ABIS_DATA_EXPORT_URL: "ABIS_DATA_EXPORT_URL",
      CORE_SWAGGER_DESCRIPTION: "CORE_SWAGGER_DESCRIPTION",
      ENABLE_CACHE: "CORE_ENABLE_CACHE",
      REDIS_URL: "CORE_REDIS_URL",
      REDIS_TTL: "CORE_REDIS_TTL",
      NODE_OPTIONS: "CORE_MAX_HEAP_SIZE",
      RESET_ADMIN_USER_PASS: "RESET_ADMIN_USER_PASS",
      CORE_SLACK_WEBHOOK_URL: "CORE_SLACK_WEBHOOK_URL",
      CORE_MAX_PAYLOAD_SIZE_MB: "CORE_MAX_PAYLOAD_SIZE_MB",
      CORE_PUSH_VAPI_PRIVATE_KEY: "PUSH_VAPI_PRIVATE_KEY",
      CORE_PUSH_VAPI_PUBLIC_KEY: "PUSH_VAPI_PUBLIC_KEY"
    },
    webapp: {
      VUE_APP_SENTRY_DSN: "WEBAPP_SENTRY_DSN",
      ADMIN_ROLE_TYPE: "ADMIN_ROLE_TYPE",
      VUE_APP_ENABLE_SENTRY_DIALOG: "VUE_APP_ENABLE_SENTRY_DIALOG",
      VUE_APP_MAPS_KEY: "VUE_APP_MAPS_KEY",
      VUE_APP_DEPLOYED_ENV_NAME: "VUE_APP_DEPLOYED_ENV_NAME",
      VUE_APP_SPOOF_PLOT_LAYOUT: "VUE_APP_SPOOF_PLOT_LAYOUT",
      VUE_APP_DEV_BARCODE: "VUE_APP_DEV_BARCODE",
      VUE_APP_GEOCODING_API_URL_BASE: "VUE_APP_GEOCODING_API_URL_BASE",
      VUE_APP_GEOCODING_MAP_KEY: "VUE_APP_GEOCODING_MAP_KEY",
      VUE_APP_ENABLE_OIDC: "VUE_APP_ENABLE_OIDC",
      VUE_APP_OIDC_AUTH_TYPE: "VUE_APP_OIDC_AUTH_TYPE",
      VUE_APP_OIDC_CLIENT: "VUE_APP_OIDC_CLIENT",
      VUE_APP_OIDC_DISCOVERY_URI: "VUE_APP_OIDC_DISCOVERY_URI",
      VUE_APP_OIDC_CLIENT_ID: "VUE_APP_OIDC_CLIENT_ID",
      VUE_APP_OIDC_SCOPES: "VUE_APP_OIDC_SCOPES",
      VUE_APP_OIDC_LOGIN_REDIRECT_URI: "VUE_APP_OIDC_LOGIN_REDIRECT_URI",
      VUE_APP_OIDC_LOGOUT_REDIRECT_URI: "VUE_APP_OIDC_LOGOUT_REDIRECT_URI",
      ORG_ENABLE_TOKEN_VALIDATION: "ORG_ENABLE_TOKEN_VALIDATION",
      CORE_API_PREFIX: "CORE_API_PREFIX",
      ORG_API_PREFIX: "ORG_API_PREFIX",
      ORG_CUSTOM_API_PREFIX: "ORG_CUSTOM_API_PREFIX",
      VUE_APP_MODE_OF_OPERATION: "VUE_APP_MODE_OF_OPERATION",
      VUE_APP_ENABLE_PROJECT_PLOT_SHARING: "VUE_APP_ENABLE_PROJECT_PLOT_SHARING",
      VUE_APP_RESOURCE_URL: "VUE_APP_RESOURCE_URL",
      VUE_APP_AWS_BUCKET: "VUE_APP_AWS_BUCKET",
      VUE_APP_AWS_REGION: "VUE_APP_AWS_REGION",
      VUE_APP_AWS_ACCESS_KEY_ID: "VUE_APP_AWS_ACCESS_KEY_ID",
      VUE_APP_AWS_SECRET_ACCESS_KEY: "VUE_APP_AWS_SECRET_ACCESS_KEY",
      VUE_APP_RC: "VUE_APP_RC",
      VUE_APP_WEB_PUSH_PUBLIC_KEY: "PUSH_VAPI_PUBLIC_KEY"
    },
    exporter: {
      LOG_LEVEL: "EXPORTER_LOG_LEVEL",
      FLASK_RUN_PORT: "FLASK_RUN_PORT",
      FLASK_ENV: "DNS_WEBAPP",
      FLASK_DEBUG: "FLASK_DEBUG",
      ENABLE_CACHE: "EXPORTER_ENABLE_CACHE",
      CACHE_REDIS_HOST: "CACHE_REDIS_HOST",
      CACHE_REDIS_PORT: "CACHE_REDIS_PORT",
      CACHE_REDIS_URL: "CACHE_REDIS_URL",
      CACHE_DEFAULT_TIMEOUT: "CACHE_DEFAULT_TIMEOUT",
      ENABLE_UPLOAD: "EXPORTER_ENABLE_UPLOAD",
      AWS_BUCKET: "EXPORTER_AWS_BUCKET",
      AWS_ACCESS_KEY_ID: "EXPORTER_AWS_ACCESS_KEY_ID",
      AWS_SECRET_ACCESS_KEY: "EXPORTER_AWS_SECRET_ACCESS_KEY",
      AWS_RESOURCE_BUCKET: "EXPORTER_AWS_RESOURCE_BUCKET",
      FILE_ROOT_PATH: "FILE_ROOT_PATH",
      AGENT: "EXPORTER_AGENT",
      ENABLE_SHACL_VALIDATOR: "ENABLE_SHACL_VALIDATOR",
      PROTOCOL_SETS: "PROTOCOL_SETS",
      VOCAB_SERVER: "VOCAB_SERVER",
      REUSABLE_CONCEPTS: "REUSABLE_CONCEPTS",
      RESOURCE_DIRECTORY: "EXPORTER_RESOURCE_DIRECTORY",
      ORG_URL: "DNS_PARATOO_ORG",
      CORE_URL: "CORE_URL",
      RANDOM_JWT: "EXPORTER_JWT",
      EXPORTER_JWT: "EXPORTER_JWT",
      SENTRY_DNS: "EXPORTER_SENTRY_DSN",
    },
  };

  // create .env file for related service
  const relatedMap = envMap[service];

  // need this one to do the update as docker swarm update command doesn't look at the yml file or the .env file either
  // so we need to concat the values to the cli command
  let updateServiceArg = "";

  // the env file content required to bring up the stack
  let envFileContent = `#### AUTOGENERATED DO NOT EDIT ######`;

  for (const [mappedKey, originalKey] of Object.entries(relatedMap)) {
    const valToUse = process.env[originalKey];
    if (valToUse) {
      process.env[mappedKey] = valToUse;
      envFileContent += `\n${mappedKey}=${valToUse}`;
      updateServiceArg += ` --env-add \"${mappedKey}=${valToUse}\"`;
    } else {
      console.warn(
        `${originalKey} env not found in both local.env and .env, will use default value if possible`
      );
    }
  }

  // there some special env variables derive from others
  envFileContent = appendDerivedEnvVariables(service, envFileContent);

  const envFilePath = `${__dirname}/monitor-${service}.env`;
  await fs.writeFile(envFilePath, envFileContent, "utf-8");

  return updateServiceArg;
}

function appendDerivedEnvVariables(service, envFileContent) {
  // strip the prefix from DNS as some variables only need the DNS without the prefix
  const regex = /^(https?:)?\/\//;
  const dnsWithoutPrefix = {
    CORE: process.env.DNS_PARATOO_CORE.replace(regex, ""),
    ORG: process.env.DNS_PARATOO_ORG.replace(regex, ""),
    WEBAPP: process.env.DNS_WEBAPP.replace(regex, ""),
  };

  switch (service) {
    case "core": {
      const swaggerServer = `https://${dnsWithoutPrefix["CORE"]}/api`;
      process.env.CORE_SWAGGER_SERVER = swaggerServer;
      envFileContent += `\nCORE_SWAGGER_SERVER=${swaggerServer}`;

      // aws log group's name
      const awsLogGroup = `${
        process.env.IMAGE_ENV?.replace("/", "") || ""
      }-monitor`;
      process.env.LOG_GROUP = awsLogGroup;
      envFileContent += `\nLOG_GROUP=${awsLogGroup}`;
      break;
    }
    case "exporter": {
      const envName = dnsWithoutPrefix["WEBAPP"] || dnsWithoutPrefix["ORG"];
      process.env.EXPORTER_DEPLOYED_ENV_NAME = envName;
      envFileContent += `\nEXPORTER_DEPLOYED_ENV_NAME=${envName}`;
      break;
    }

    case "webapp": {
      // in older version CORE's and ORG's DNS variable do not have http and https, but the new version do
      //  it's to let caddy know which mode to operate, as in prod we're using WAF with load balance which will handle https cert
      //  that why we don't need caddy to handle tls cert for us but only act as a reverse proxy
      const backends = ["CORE", "ORG"];
      for (const backend of backends) {
        const variableName = `${backend}_API_URL_BASE`;
        process.env[variableName] = `https://${dnsWithoutPrefix[backend]}`;
        envFileContent += `\n${variableName}=https://${dnsWithoutPrefix[backend]}`;
      }
      break;
    }

    default:
      throw new Error(`service ${key} not found`);
  }

  return envFileContent;
}

await init();
