export S3_BUCKET=your-bucket-name
# optional if the instance is an EC2 instance with assigned role that has access to related S3 bucket
# export AWS_ACCESS_KEY_ID=blas
# export AWS_SECRET_ACCESS_KEY=blas
export AWS_DEFAULT_REGION=ap-southeast-2

export CLONE_DB_HOST=localhost
export CLONE_DB_USERNAME=user
export CLONE_DB_PASSWORD=password
export CLONE_DB_NAME=core
export CLONE_DB_PORT=45432

# optional
export SLACK_WEBHOOK_URL=url

export REPO_FOLDER=/home/ubuntu/paratoo-fdcp-runner
