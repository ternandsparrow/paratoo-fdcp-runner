#!/bin/bash
# Append the current PATH to env.sh
echo -e "\nexport PATH=$PATH" >>env.sh

# Set the log file path
log_file=$(pwd)/rds_clone.log
if [ ! -f "$log_file" ]; then
  touch $log_file
fi

# Add a cron job that runs the script every hour at minute 1
(
  crontab -l 2>/dev/null
  echo "0 14 * * SAT $(pwd)/rds_clone.sh >> $log_file 2>&1"
) | crontab -
