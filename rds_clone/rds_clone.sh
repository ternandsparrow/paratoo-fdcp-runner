#!/bin/bash
set -o pipefail
SCRIPT_DIR=$(dirname $(readlink -f $0))
source "$SCRIPT_DIR/env.sh"

if [ ! -z "$DEBUG" ]; then
  trap "cat /tmp/command_error.log" EXIT
fi
echo "Starting cloning at $(date)"

start=$(date +%s)
cd $REPO_FOLDER
# bring the stack down
echo y | yarn node monitor.mjs down || exit 1
run_cmd_for_target_db() {
  PGPASSWORD="$CLONE_DB_PASSWORD" psql -h "$CLONE_DB_HOST" -p "$CLONE_DB_PORT" -U "$CLONE_DB_USERNAME" "$@"
}
tmp_dump=$(mktemp)
clone_db() {
  echo "get latest snapshot"
  latest=$(
    aws s3api list-objects-v2 --bucket $S3_BUCKET --query 'Contents | sort_by(@, &LastModified)[-1].Key' --output text 2>/tmp/command_error.log || return 1
  )
  aws s3 cp s3://$S3_BUCKET/$latest $tmp_dump 2>/tmp/command_error.log || return 1

  drop_db_query="DROP DATABASE IF EXISTS ${CLONE_DB_NAME}_old;"
  run_cmd_for_target_db -c "$drop_db_query" -d postgres 2>/tmp/command_error.log || return 1

  rename_db_query="ALTER DATABASE $CLONE_DB_NAME RENAME TO ${CLONE_DB_NAME}_old;"
  run_cmd_for_target_db -c "$rename_db_query" -d postgres 2>/tmp/command_error.log || return 1

  create_db_query="CREATE DATABASE $CLONE_DB_NAME;"
  run_cmd_for_target_db -c "$create_db_query" -d postgres 2>/tmp/command_error.log || return 1

  echo "Cloning"
  export PGPASSWORD="$CLONE_DB_PASSWORD"
  pg_restore -Fc --no-owner -U "$CLONE_DB_USERNAME" \
    -d $CLONE_DB_NAME -p "$CLONE_DB_PORT" \
    -h "$CLONE_DB_HOST" $tmp_dump 2>/tmp/command_error.log || return 1

  echo "Cloning done"
}

send_message() {
  current_date=$(date "+%Y-%m-%d %H:%M:%S")
  echo "Sending message: $1"
  # strip double quotes and replace with single to create json string
  msg="[$current_date] ${1//\"/\'}"
  data="{\"text\":\"$msg\"}"
  curl -X POST -H 'Content-type: application/json' --data "$data" "$SLACK_WEBHOOK_URL"
}
#  clone db and return error message if any error if AWS_SNS_ARN is set
clone_db
ressult=$?
rm $tmp_dump
end=$(date +%s)
runtime=$((end - start))
msg="Cloning successful! Took $runtime seconds to clone"

if [ $ressult -eq 1 ]; then
  msg="Cloning failed: $(cat /tmp/command_error.log)"
fi

# bring the stack back
echo y | yarn node monitor.mjs || exit 1

# send an SNS message
if [ -n "$SLACK_WEBHOOK_URL" ]; then
  send_message "$msg"
fi

echo "$msg"
