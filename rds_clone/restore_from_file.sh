#!/bin/bash

if [ -z $1 ]; then
  echo "Must provide file to restore from"
  exit 1
fi

dump_file_location="$1"

echo "Will restore from: $dump_file_location"

set -o pipefail
SCRIPT_DIR=$(dirname $(readlink -f $0))
source "$SCRIPT_DIR/env.sh"

if [ ! -z "$DEBUG" ]; then
  trap "cat /tmp/command_error.log" EXIT
fi
echo "Starting cloning at $(date)"

start=$(date +%s)
cd $REPO_FOLDER
# bring the stack down
echo y | yarn node monitor.mjs down || exit 1

run_cmd_for_target_db() {
  PGPASSWORD="$CLONE_DB_PASSWORD" psql -h "$CLONE_DB_HOST" -p "$CLONE_DB_PORT" -U "$CLONE_DB_USERNAME" "$@"
}

restore_db() {
  set -x  #start command tracing
  echo "Dropping if exists"
  drop_db_query="DROP DATABASE IF EXISTS ${CLONE_DB_NAME}_old;"
  run_cmd_for_target_db -c "$drop_db_query" -d postgres 2>/tmp/command_error.log || return 1

  echo "Renaming"
  rename_db_query="ALTER DATABASE $CLONE_DB_NAME RENAME TO ${CLONE_DB_NAME}_old;"
  run_cmd_for_target_db -c "$rename_db_query" -d postgres 2>/tmp/command_error.log || return 1

  echo "Creating"
  create_db_query="CREATE DATABASE $CLONE_DB_NAME;"
  run_cmd_for_target_db -c "$create_db_query" -d postgres 2>/tmp/command_error.log || return 1

  echo "Cloning"
  export PGPASSWORD="$CLONE_DB_PASSWORD"

  pg_restore -Fc --no-owner -U "$CLONE_DB_USERNAME" \
    -d "$CLONE_DB_NAME" -p "$CLONE_DB_PORT" \
    -h "$CLONE_DB_HOST" $dump_file_location 2>/tmp/command_error.log || return 1
  set -x  #stop command tracing

  echo "Done cloning"
}

restore_db