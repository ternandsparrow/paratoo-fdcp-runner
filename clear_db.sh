#!/bin/bash
set -e
# DANGER!!!!!!
# This script is used to rename the core database to old_core
# and create a fresh new "core"
# this may possibly result in a loss of data
# please use with caution !!!!
SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

DB_USER=$(grep DB_USER $SCRIPT_DIR/local.env | cut -d = -f2)
DB_HOST=$(grep CORE_DB_HOST $SCRIPT_DIR/local.env | cut -d = -f2)
export PGPASSWORD=$(grep DB_PASSWORD $SCRIPT_DIR/local.env | cut -d = -f2)

read -p "[DANGER!!!!] Are you sure[y/n]? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
  psql -U $DB_USER --host $DB_HOST --command='DROP DATABASE old_core;'
  psql -U $DB_USER --host $DB_HOST --command 'alter database core rename to old_core;'
  wait
  psql -U $DB_USER --host $DB_HOST --command='CREATE DATABASE core;'
else
  exit
fi
