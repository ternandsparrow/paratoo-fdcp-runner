#!/usr/bin/env -S bash -c 'cd "$(dirname "$0")" && yarn node "$(basename "$0")" $@'
"use strict";
import { config } from "dotenv";
import "../misc/log.mjs";
import run from "../misc/run.mjs";
import fs from "fs/promises";
import path from "path";

const param = process.argv[3];
let stackName;
let stackConfigs;

async function init() {
  await isAwsCliInstalled();
  await checkArgument();
  checkConfigFile();
  await checkEc2InstanceType();
  const cfStackParams = makeCFParams();
  await syncTemplatesToS3();
  await checkStackStatus();
  console.log(" Deploying stack...");
  await deploy(cfStackParams);
  console.log(" Done!");
}

async function isAwsCliInstalled() {
  try {
    await run(`aws --version`);
  } catch (err) {
    console.error(
      `aws cli not installed, If you want to use this script, please install aws cli first`
    );
    console.log(`for Debian based os, run: sudo apt install awscli`);
    process.exit(1);
  }
}

async function checkArgument() {
  if (!param) {
    console.error("Invalid argument. Exiting.");
    process.exit(1);
  } else {
    const configPath = path.join(__dirname, "configs", `${param}.env`);
    console.log(configPath);
    const isExistConfig = await fs.pathExists(configPath);
    if (!isExistConfig) {
      console.error("Config not found. Exiting.");
      process.exit(1);
    }
    config({ path: configPath });
    console.log(process.env.STACK_NAME);
  }
}

// check required params
function checkConfigFile() {
  stackName = process.env.STACK_NAME;
  stackConfigs = {
    // Version: process.env.VERSION || 1,
    EnvironmentStage: process.env.ENVIRONMENT_STAGE || "dev", // [optional] default to 'dev'
    // ============ EC2 configs ====================
    VPCClassACidr: process.env.VPC_CLASS_A_CIDR,
    VPCClassBCidr: process.env.VPC_CLASS_B_CIDR,
    SSHCidr: process.env.SSH_CIDR,
    EC2InstanceType: process.env.EC2_INSTANCE_TYPE || "m7a.large",
    //  [optional] will default to ubuntu-jammy-22.04-amd64-server-20231207 : ami-04f5097681773b989
    EC2AMIId: process.env.EC2_AMI_ID,
    EC2SnapshotName: process.env.EC2_SNAPSHOT_NAME,
    EC2KeyPairName: process.env.EC2_KEYPAIR_NAME,
    // [optional] Number of docker swarm manger and worker , default to 1 if not specified
    ManagerCount: process.env.MANAGER || 1,
    WorkerCount: process.env.WORKER || 1,
    ManagerJoinToken: process.env.MANAGER_JOIN_TOKEN || null,
    WorkerJoinToken: process.env.WORKER_JOIN_TOKEN || null,
    // ============ Pg db cluster ====================
    // [optional if no snapshot] DB username and password are required if no DB snapshot is used, otherwise optional
    RDSMaxCapacity: process.env.RDS_MAX_CAPACITY,
    DBUsername: process.env.DB_USERNAME,
    DBPassword: process.env.DB_PASSWORD,
    // [optional] If DBSnapshotName is specied, DBUserName and DBPassword will be optional
    DBSnapshotName: process.env.DB_SNAPSHOT_NAME,
    // [optional] default to 15.4 if not specify
    DBEngineVersion: process.env.DB_ENGINE_VERSION || "15.4",
  };
  const requiredParams = ["SSHCidr", "EC2KeyPairName"];

  if (!stackName) {
    console.error(`STACK_NAME is required`);
    process.exit(1);
  }
  const awsCliEnv = [
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY",
    "AWS_DEFAULT_REGION",
  ];
  for (const env of awsCliEnv) {
    if (!process.env[env]) {
      console.error(`${env} is required`);
      process.exit(1);
    }
  }
  for (const param of requiredParams) {
    if (!stackConfigs[param]) {
      console.error(`${param} is required`);
      process.exit(1);
    }
  }
  if (
    !stackConfigs.DBSnapshotName &&
    !stackConfigs.DBUsername &&
    !stackConfigs.DBPassword
  ) {
    console.error(`DBSnapshotName or DBUsername and DBPassword are required`);
    process.exit(1);
  } else if (
    stackConfigs.DBSnapshotName &&
    (stackConfigs.DBUsername || stackConfigs.DBPassword)
  ) {
    console.error(
      `DBSnapshotName and DBUsername and DBPassword are mutually exclusive`
    );
    process.exit(1);
  }
}
// TODO: update this function
// function helpInfo(exitCode = 0) {
//   console.log(`
//     To deploy the full stack (vpc + ec2 + aurorapg),
//     s3 is not included as other stack does not depend on it
//       ${cmd} full
//     To deploy vpc only
//       ${cmd} vpc
//     To deploy s3 only
//       ${cmd} s3
//     To deploy ec2 manager only (required an existing vpc stack)
//       ${cmd} ec2-manager
//     To deploy ec2 worker (required an existing vpc stack and ec2 manager)
//       ${cmd} ec2-worker
//     To deploy aurora cluster with postgres database only (required an existing vpc stack)
//       ${cmd} aurorapg
//     `)
//   process.exit(exitCode)
// }

/**
 * some instance types are not exist in some certain regions
 */
async function checkEc2InstanceType() {
  try {
    const p = await run(`aws ec2 describe-instance-types \
          --output json \
          --filters Name=instance-type,Values=${stackConfigs.EC2InstanceType}`);
    const res = JSON.parse(p.stdout).InstanceTypes;
    if (res.length === 0) {
      console.error(
        `EC2 instance type ${stackConfigs.EC2InstanceType} does not exist in your region`
      );
    }
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

function makeCFParams() {
  let stackParams = `TemplateBucket=${stackName.toLowerCase()}-templates`;
  for (const [key, value] of Object.entries(stackConfigs)) {
    if (value) {
      stackParams += ` ${key}=${value}`;
    }
  }

  if (process.env.DRY_RUN) {
    console.log(stackParams);
  }

  return stackParams;
}

async function checkStackStatus(stack = stackName) {
  if (process.env.DRY_RUN) return;
  while (true) {
    try {
      console.log(` Checking status of ${stack}...`);
      const p = await run(`aws cloudformation describe-stacks\
                            --stack-name ${stack}\
                            --output json`);
      const status = JSON.parse(p.stdout).Stacks[0].StackStatus;

      switch (status) {
        case "UPDATE_IN_PROGRESS":
          console.error(`Cannot update the stack, it is in UPDATE_IN_PROGRESS state,
    if you want to force update, please cancel it by this command and run this script again:
    "aws cloudformation cancel-update-stack --stack-name ${stackName}"`);
          process.exit(1);

        case "ROLLBACK_COMPLETE":
          console.log(
            ` The stack is in ROLLBACK_COMPLETE state, cleaning up...`
          );
          await deleteStack();
          continue;

        case "CREATE_IN_PROGRESS":
        case "ROLLBACK_IN_PROGRESS":
        case "UPDATE_COMPLETE_CLEANUP_IN_PROGRESS":
        case "UPDATE_ROLLBACK_IN_PROGRESS":
        case "UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS":
        case "DELETE_IN_PROGRESS":
          console.log(
            ` The stack is in ${status} state, attempt to check again in 30 seconds...`
          );
          await new Promise((res) => setTimeout(res, 30000));
          continue;

        default:
          if (!status && stackConfigs.ManagerJoinToken) {
            throw new Error(
              "The current stack does not exist, so the ManagerJoinToken you provided is invalid. Please check again."
            );
          } else return;
      }
    } catch (error) {
      const errorMsg = error.stderr;
      const stackNotExistMsg = `Stack with id ${stack} does not exist`;

      if (errorMsg?.includes(stackNotExistMsg)) {
        if (stackConfigs.ManagerJoinToken) {
          console.error(
            "The current stack does not exist, so the ManagerJoinToken you provided is invalid. Please check again."
          );
          process.exit(0);
        } else return;
      } else {
        console.log("error", error);
        process.exit(1);
      }
    }
    process.exit(1);
  }
}

async function deleteStack(stack = stackName) {
  try {
    await run(`aws cloudformation delete-stack --stack-name ${stack}`);
  } catch (error) {
    console.log(error);
  }
}

async function syncTemplatesToS3() {
  const res = await run(`aws configure get region --output text`);
  const bucketName = stackName.toLowerCase() + "-templates";
  const region = res.stdout.trim();
  try {
    await run(`aws s3api head-bucket --bucket ${bucketName}`);
  } catch (error) {
    const errMsg = error.stderr;
    if (errMsg.includes("404")) {
      await createBucket(bucketName, region);
    }
  }
  try {
    await run(
      `aws s3 cp ${__dirname}/template/ s3://${bucketName}/ --recursive --exclude "*" --include "*.yml"`
    );
  } catch (error) {
    console.error("error", error);
  }
}
async function createBucket(bucketName, region) {
  try {
    await run(`aws s3api create-bucket\
             --bucket ${bucketName} \
             --region ${region}\
             --create-bucket-configuration LocationConstraint=${region}`);
  } catch (error) {
    const errMsg = error.stderr;
    if (errMsg.includes("IllegalLocationConstraintException")) {
      console.error("S3 bucket' name already exists");
      process.exit(1);
    }
  }
}

async function deploy(params, stack = stackName) {
  // s3 bucket required lowercase name, so we need to convert it
  let template = "full-stack";

  await checkKeyPair();

  if (process.env.DRY_RUN) {
    console.log(`
    aws cloudformation deploy \r
        --stack-name ${stack} \r
        --template-file ${__dirname}/template/${template}.yml \r
        --parameter-overrides ${params} \r
        --capabilities CAPABILITY_NAMED_IAM 
    `);
    return;
  }
  try {
    await run(`
    PARAMS=${params}
    aws cloudformation deploy \
      --stack-name ${stack} \
      --template-file ${__dirname}/template/${template}.yml \
      --parameter-overrides $PARAMS \
      --capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND
    `);
  } catch (error) {
    console.error("Deploy stack failed", error);
    await run(`
      aws cloudformation describe-stack-events \
          --stack-name ${stackName} \
          --output json |
      #pipe error to error.json and stdout
      tee error.json
      `);
    const errorFile = path.join(__dirname, "error.json");
    console.log(`Check error details in ${errorFile}`);
    process.exit(1);
  }
}

async function checkKeyPair() {
  try {
    const EC2KeyPairName = stackConfigs.EC2KeyPairName || "";
    const p = await run(`
    aws ec2 describe-key-pairs \
        --output json \
        --filters Name=key-name,Values="${EC2KeyPairName}"
    `);
    const res = JSON.parse(p.stdout).KeyPairs;
    if (res.length === 0) {
      console.error(`EC2 key pair ${EC2KeyPairName} does not exist`);
      process.exit(1);
    }
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

init();
